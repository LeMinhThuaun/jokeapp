
import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import {
    CloseOutlined,
    MenuOutlined,
} from '@ant-design/icons';

import 'bootstrap/dist/css/bootstrap.min.css';

import './style.scss';
import logo from './logo.png'
import avatar from './ava.jpg'

import { data_fake } from './datafake'


const JokeApp = () => {

    const [menu, setMenu] = useState(false)
    const [data, setData] = useState({
        dataList: [],               //data lấy về
        dataShow: null,             //data show ra view
        position: 0,                //joke thứ mấy đc show
        dataCountFunny: [],       //tính joke funny
        dataCountNotFunny: [],    //tính joke k funny

        disabled: false,            //khóa 2 nút khi hết joke
    })

    //show joke đầu tiên
    useEffect(() => {
        if (data.dataList) {
            setData({
                ...data,
                dataShow: data.dataList[data.position]
            })
        }
    }, [data.dataList])

    useEffect(() => {
        if (data.dataList) {
            if (data.position === data.dataList.length) {
                setData({
                    ...data,
                    disabled: true
                })
            }
            else {
                setData({
                    ...data,
                    dataShow: data.dataList[data.position]
                })
            }

        }
    }, [data.position])

    //lấy data về
    useEffect(() => {
        if (data_fake) {
            setData({
                ...data,
                dataList: data_fake,
            })
        }
    }, [data_fake])

    //sự kiện click for Funny joke
    const onClickFunny = (e) => {   //lấy id joke
        const count = [].concat(data.dataCountFunny)
        count.push(e)
        if (data.position < data.dataList?.length) {
            setData({
                ...data,
                dataCountFunny: count,  //lưu list id joke để biết joke nào funny
                position: data.position + 1,
            })
        }
    }

    //sự kiện click for Not Funny joke
    const onClickNotFunny = (e) => {
        const count = [].concat(data.dataCountNotFunny)
        count.push(e)

        if (data.position < data.dataList?.length) {
            setData({
                ...data,
                dataCountNotFunny: count, //lưu list id joke để biết joke nào not funny
                position: data.position + 1,
            })
        }
    }

    return (
        <div className="joke">
            <div className="container">
                <div className="joke__header">
                    <div className="joke__logo">
                        <img src={logo} alt='logo'></img>
                    </div>
                    <div className="joke__authen ">
                        {
                            menu === false ?

                                <MenuOutlined className='joke__menu-mb' onClick={() => setMenu(true)} />
                                :
                                <CloseOutlined className='joke__menu-mb' onClick={() => setMenu(false)} />
                        }
                        <div className={`joke__menu-pc ${menu === true ? 'active' : ''}`}>
                            <div className="desc">
                                <div className="position">Handicrafted by</div>
                                <div className="name">Jim HLS</div>
                            </div>
                            <div className="avatar">
                                <img src={avatar} alt='avatar'></img>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div className="joke__banner">
                <h2>A joke a day keeps the docter away</h2>
                <p className="desc">If you joke wrong way, your teeth have to pay. (Serious)</p>
            </div>
            <div className='container joke__container_wrap'>
                <div className="joke__main-content">
                    <p>
                        {
                            data.position === data.dataList.length ?
                                "That's all the jokes for today! Come back another day!" :
                                data.dataShow?.content
                        }
                    </p>
                </div>
                <div className="joke__action">
                    <Button disabled={data.disabled} className='btn btn__blue mr-2' onClick={() => onClickFunny(data.dataShow?.id)}>This is Funny!</Button>
                    <Button disabled={data.disabled} className='btn btn__green' onClick={() => onClickNotFunny(data.dataShow?.id)}>This is not funny.</Button>
                </div>
                <hr></hr>
                <div className='joke__footer'>
                    <p>
                        This website is created as part of Hlsolutions program. The materials contained on this website are provided for general
                        information only and do not constitute any form ofadvice. HLS assumes no responsibility for the accuracy of any particular statement and
                        accepts no liability for any loss or damage which may arise from reliance on the information contained on this site.
                    </p>
                    <span>Copyright 2021 HLS</span>
                </div>
            </div>
        </div >
    );
}

export default JokeApp;
